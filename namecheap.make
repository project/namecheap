; Drush Make (http://drupal.org/project/drush_make)
api = 2
core = 7.x

projects[namecheap][type] = library
projects[namecheap][download][type] = file
projects[namecheap][download][url] = http://namecheap.googlecode.com/files/namecheap-1.1.8.zip

projects[libraries] = 2.0-alpha2
